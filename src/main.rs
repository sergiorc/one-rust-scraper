#[macro_use]
extern crate clap;

extern crate reqwest;
extern crate select;

use select::document::Document;
use select::predicate::Name;
use std::error::Error;

fn main() {       
    // Parse arguments
    let matches = clap_app!(myapp =>
        (version: "1.0")
        (author: "Sergio RC <sergiorc@yandex.com>")
        (about: "Does awesome things")
        (@arg URL: -u --url +required +takes_value "Target url to be visited")
        (@arg debug: -d ... "Sets the level of debugging information")
    ).get_matches();
    
    // Read -u,--url argument and pass its value to ini the request
    let url: &str = matches.value_of("URL").unwrap();
    make_request(url);    
}



fn make_request(url: &str) {   
    // Init request to url
    match reqwest::get(url) {
        // It works
        Ok(mut response) => {            
            if response.status() == reqwest::StatusCode::Ok {        
                let text = response.text().unwrap_or("notext".to_string());                               
                let thelinks = get_links(&text).unwrap();  
                let p = get_paragraphs(&text).unwrap();

                println!("Got {} links", thelinks.len());                               
                println!("Got {} paragraphs", p.len());
                
                // TODO: usar hilos para visitar cada link
                
            } else {
                println!("content error: {}", response.status());
            }
        },
        // Something is wrong
        Err(err) => {
            println!("url: {}", url);
            println!("Request failed: {}", err)
        }
    }
}

fn get_links(text: &String) -> Result<Vec<String>, Box<Error>> {      
    let mut links: Vec<String> = Vec::new();  
    Document::from(text.as_str())
        .find(Name("a"))
        .filter_map(|x| x.attr("href"))
        .for_each(|x| links.push(String::from(x)));
    Ok(links)
}

fn get_paragraphs(text: &String) -> Result<Vec<String>, Box<Error>> {
    Ok(
        Document::from(text.as_str())
        .find(Name("p"))
        .map(|node| node.text())
        .collect::<Vec<String>>()
    )
}

