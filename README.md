# one-rust-scraper (This is a work in progress)
Simple web scraper using Rust language

# Installation
1. cd one-rust-scraper
2. cargo run

# Usage
1. Open terminal and type: one-rust-scraper --url TARGET URL
